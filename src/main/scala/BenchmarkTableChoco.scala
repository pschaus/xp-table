/*******************************************************************************
  * OscaR is free software: you can redistribute it and/or modify
  * it under the terms of the GNU Lesser General Public License as published by
  * the Free Software Foundation, either version 2.1 of the License, or
  * (at your option) any later version.
  *
  * OscaR is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU Lesser General Public License  for more details.
  *
  * You should have received a copy of the GNU Lesser General Public License along with OscaR.
  * If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
  ******************************************************************************/


import org.chocosolver.solver.Solver
import org.chocosolver.solver.variables._
import org.chocosolver.solver.search.strategy.selectors._;
import org.chocosolver.solver.search.strategy.selectors.variables._;
import org.chocosolver.solver.search.strategy.selectors.values._;
import org.chocosolver.solver.search.strategy._;
import org.chocosolver.solver.constraints._;
import org.chocosolver.solver.trace._;

import oscar.cp._

import scala.xml.XML

/**
 * Object to solve a specific instance :
 *      http://www.cril.univ-artois.fr/~lecoutre/benchmarks.html
 * Usage:
 *      scala BenchmarkTable instance_file.xml [nb_sols] algorithm
 * where algorithm is one of the following :
 *      CT for Compact Table
 *      STR2 for STR2
 *      STR3 for STR3
 *      GAC4 for GAC-4
 *      GAC4R for GAC-4R
 *      MDD4R for MDD-4R
 *
 *  @author Jordan Demeulenaere and Pierre Schaus
 */
object BenchmarkTableChoco extends App {
  val TIME_OUT = 1000


  val label = args(0)
  val instance = args(1)
  val implem = args(2)


  import oscar.cp.constraints.tables.TableAlgo._
  import oscar.cp.constraints.tables._

  def makeTupleObject(t: Array[Array[Int]]) = {
    val tuples = new org.chocosolver.solver.constraints.extension.Tuples(true);
    for (tuple <- t) tuples.add(tuple:_*)
    tuples
  }

  val implems = Map(
    //"CT" -> ((x: Array[CPIntVar], tuples: Array[Array[Int]]) => table(x, tuples,CompactTable)),
    "STR2" -> ((x: Array[org.chocosolver.solver.variables.IntVar], tuples: Array[Array[Int]]) => IntConstraintFactory.table(x,makeTupleObject(tuples),"STR2+"))
  )

  val solver = new Solver("my first problem");




  //implicit val solver: CPSolver = CPSolver()
  val xml = XML.loadFile(instance)

  /* Domains of the variables */
  val domains = (xml \ "domains" \ "domain").map { line =>
    val name = (line \ "@name").text
    val nbValues = (line \ "@nbValues").text.toInt

    val values = if (line.text.contains("..")) {
      val rangeBounds = line.text.split("\\.\\.").map(_.trim.toInt)
      val range = rangeBounds(0) to rangeBounds(rangeBounds.length - 1)
      range.toSet
    } else {
      line.text.trim.split(" ").map(_.toInt).toSet
    }

    assert(values.size == nbValues)
    (name, values)
  }.toMap

  /* Variables */
  val varsArray = (xml \ "variables" \ "variable").map { line =>
    val name = (line \ "@name").text
    val domain = (line \ "@domain").text

    (name, VariableFactory.enumerated("",domains(domain).toArray,solver))
  }
  val indexOfVar = varsArray.map(_._2).zipWithIndex.toMap

  val vars = varsArray.toMap


  /* Tuples */
  val relations = (xml \ "relations" \ "relation").map { line =>
    val name = (line \ "@name").text
    val nbTuples = (line \ "@nbTuples").text.toInt
    val tuples = line.text.trim.split("\\|").map(tup => tup.trim.split(" ").map(_.trim.toInt))
    (name, tuples)
  }.toMap

  var beforeInit = 0L
  var endInit = 0L
  var beforeSearch = 0L
  var finalTime = 0L
  try {
    /* Constraints */
    val constraints = (xml \ "constraints" \ "constraint").map { line =>
      val name = (line \ "@name").text
      val scopeName = (line \ "@scope").text.split(" ")
      val scope = scopeName.map(vars(_))
      val tab = relations((line \ "@reference").text)

      (name, scope, tab)
    }

    /* We add the constraints */
    beforeInit = System.currentTimeMillis()
    val theConstraints = constraints.map(p => implems(implem)(p._2,p._3))
    for (c <- theConstraints) {
      solver.post(c)
    }


    endInit = System.currentTimeMillis()

    /* Search for a solution */
    val X = varsArray.map(_._2).toArray

    var degree = Array.fill(X.size)(0)

    val varIndex = X.zipWithIndex.toMap

    constraints.foreach { c =>
      for (x <- c._2) {
        degree(varIndex(x)) += 1
      }
    }

    val Y = X.sortBy(x => - degree(varIndex(x))*100/x.getDomainSize()  )


    class DegreeOverWeight extends VariableSelector[IntVar] with VariableEvaluator[IntVar] {

      def getVariable(variables: Array[IntVar]): IntVar = {
        var small_idx: Int = -1
        var minDegOverWeight: Int = Integer.MAX_VALUE
        var idx: Int = 0
        while (idx < variables.length) {
          {
            val dsize: Int = variables(idx).getDomainSize
            val degOverWeight =  - degree(idx)<< 7 /dsize
            if (dsize > 1 && degOverWeight < minDegOverWeight) {
              minDegOverWeight = degOverWeight
              small_idx = idx
            }
          }
          ({
            idx += 1; idx - 1
          })
        }
        return if (small_idx > -1) variables(small_idx)
        else null
      }

      def evaluate(variable: IntVar): Double = {
        return - (degree(varIndex(variable))<< 7) / variable.getDomainSize
      }
    }


    solver.set(IntStrategyFactory.custom(new DegreeOverWeight(), new IntDomainMin(), X:_*));



    beforeSearch = System.currentTimeMillis()

    solver.findSolution();


    //println(stats)

    finalTime = System.currentTimeMillis()


    val initTime = endInit - beforeInit
    val searchTime = finalTime - beforeSearch
    val totalTime = initTime + searchTime
    println(totalTime)
    Chatterbox.printStatistics(solver);
    //println(stats)
    /*
    val time = stats.time
    if (time < TIME_OUT * 1000) {
      println(s"$label\t${instance}\t$implem\t$initTime\t$searchTime\t$totalTime\t${stats.nFails-stats.nSols}\t${stats.nNodes}\t${stats.nSols}")
    }
    else {
      println(s"$label\t${instance}\t$implem\t$initTime\tT.0.\tT.0.\tT.0.\tT.0.\tT.0.")
    }*/


  }
  catch {
    case e: OutOfMemoryError => println(s"$instance\tMemory Out")
    case e: oscar.cp.core.NoSolutionException => {
      val initTime = endInit - beforeInit
      val searchTime = finalTime - beforeSearch
      val totalTime = initTime + searchTime
      //println(s"${instance.substring(42)}\tNo solution")
    }
    case e: Exception => {
      println(args(1)+"\n"+ e)
      throw e
    }
  }
}
