/**
 * Created by pschaus on 27/02/16.
 */
object PerformanceProfile extends App {


  var lines = scala.io.Source.fromFile("rawresults.txt").getLines().toArray

  var memoryIssues = lines.filter(_.contains("Memory")).map(_.split("\t")(0)).toSet
  lines = lines.filter(l => memoryIssues.forall(i => !l.contains(i)))

  val TO = 1000000

  var all = (for (l <- lines) yield {
    val ls = l.split("\t")
    //println(ls.mkString(","))
    val t = if (ls(4) == "T.0.") TO else ls(4).toInt
    def toInt(s: String) = if (s == "T.0.") TO else s.toInt
    Array(ls(0),ls(1),ls(2),ls(3),toInt(ls(4)),toInt(ls(5)),toInt(ls(6)),toInt(ls(7)))
  }).toArray
  val labels = all.map(r => r(0)).toSet.zipWithIndex.toMap

  def bkts(i: Int) = all(i)(7).asInstanceOf[Int]

  def maxTime(i: Int) = (0 to 6).map(j => all(i+j)(4).asInstanceOf[Int]).max
  def atLeastOneTimeOut(i: Int) = (0 to 5).count(j => all(i+j)(4).asInstanceOf[Int]==TO) > 0

  val doNotSelectEasy = (0 until all.size by 7).filter(i => (maxTime(i) <= 2000 || bkts(i) < 500)).map(all(_)(1)).toSet
  val doNotSelectDifficult = (0 until all.size by 7).filter(i =>  atLeastOneTimeOut(i)).map(all(_)(1)).toSet

  println("#easy:"+doNotSelectEasy.size)
  println("#difficult:"+doNotSelectDifficult.size)

  val doNotSelect = (0 until all.size by 7).filter(i => (maxTime(i) <= 2000 || atLeastOneTimeOut(i) || bkts(i) < 500)).map(all(_)(1)).toSet

  //println(doNotSelect.map(_.toString).filter(_.startsWith("rand")).toArray.sortBy(i => i).mkString("\n"))


  //val doNotSelect2 = (0 until all.size by 7).filter(i => (all(i)(4).asInstanceOf[Int] <= 500 || ((0 to 6).forall(j => all(i+j)(4).asInstanceOf[Int]==TO)))).map(all(_)(1))

  //println(doNotSelect2.mkString("\n"))


  println(all.size+" all init:"+all.size.toDouble/7.0)

  all = all.filterNot(r => doNotSelect.contains(r(1)))

  println(all.size+" all init:"+all.size.toDouble/7.0)


  val instanceName = (0 until all.size by 7).map(all(_)(1)).toArray
  val timeCT = (0 until all.size by 7).map(all(_)(4)).toArray
  val timeSTR2 = (1 until all.size by 7).map(all(_)(4)).toArray
  val timeSTR3 = (2 until all.size by 7).map(all(_)(4)).toArray
  val timeGAC4 = (3 until all.size by 7).map(all(_)(4)).toArray
  val timeGAC4R = (4 until all.size by 7).map(all(_)(4)).toArray
  val timeMDD4R = (5 until all.size by 7).map(all(_)(4)).toArray
  val timeAC5TCRecomp = (6 until all.size by 7).map(all(_)(4)).toArray

  for (i <- 0 until instanceName.size) {
    println(instanceName(i)+"\t"+timeCT(i)+"\t"+timeSTR2(i)+"\t"+timeSTR3(i)+"\t"+timeGAC4(i)+"\t"+timeGAC4R(i)+"\t"+timeMDD4R(i)+"\t"+timeAC5TCRecomp(i))
  }




  /*

  val r = for ((a: Int,b: Int) <- timeGAC4 zip timeGAC4R) yield {
    a.toDouble / b.toDouble
  }

  println("minRation:"+r.min)
  println("maxRation:"+r.max)

  println(r.mkString(","))

  */


  println("number of instances:"+(all.size/7))
  //println(labels.mkString(","))
  val instances = all.map(r => labels(r(0))).sliding(1,7)

  println("---------------")
  println("{")
  println("\"metric\": \"time\",")
  println("\"labels\":"+labels.keys.mkString("[\"","\",\"","\"],"))
  println("\"instances\":"+instances.map(_.mkString("[","","]")).mkString("[",",","],"))
  println("\"data\": { ")
  println("\"CT\":{"+    (0 until all.size by 7).map(all(_)(4)).mkString("\"time\":[",",","]")  +"},")
  println("\"STR2\":{"+  (1 until all.size by 7).map(all(_)(4)).mkString("\"time\":[",",","]")  +"},")
  println("\"STR3\":{"+  (2 until all.size by 7).map(all(_)(4)).mkString("\"time\":[",",","]")  +"},")
  println("\"GAC4\":{"+  (3 until all.size by 7).map(all(_)(4)).mkString("\"time\":[",",","]")  +"},")
  println("\"GAC4R\":{"+ (4 until all.size by 7).map(all(_)(4)).mkString("\"time\":[",",","]")  +"},")
  println("\"MDD4R\":{"+ (5 until all.size by 7).map(all(_)(4)).mkString("\"time\":[",",","]")  +"},")
  println("\"AC5TCRecomp\":{"+ (6 until all.size by 7).map(all(_)(4)).mkString("\"time\":[",",","]")  +"}")
  println("}")
  println("}")


  //println(instances.mkString(","))




}
