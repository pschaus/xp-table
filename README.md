# Compact Table Benchmarker

## Description

Experimental material used for the paper

CompactTable: An efficient filtering algorithm for table constraints based on reversible sparse bit-set.
By Jordan Demeulenaere, Renaud Hartert, Christophe Lecoutre, Guillaume Perez, Laurent Perron, Jean-Charles Régin, Pierre Schaus

The instances (compressed with lzma) are in folder instances/
The scripts used for the experiments are also in instances/

Spreadsheets with speedup calculation: https://docs.google.com/spreadsheets/d/1uJulp_ImlD5WeoLZSLT0ofL98ajTkgWA1uJ3rkpzoSM/edit#gid=1431941931

The results obtained on a 32 cores machine with 100GB memory are available in rawresults.txt.
model name	: AMD Opteron(tm) Processor 6284 SE
cpu MHz		: 1400.000

Source code of table constraints available here as part of OscaR project https://bitbucket.org/oscarlib/oscar/src/tip/oscar-cp/src/main/scala/oscar/cp/constraints/tables/?at=dev

The dynamic performance profiles are available here:
https://www.info.ucl.ac.be/~pschaus/assets/publi/performance-profile-ct/index.html

## Documentation

To run locally (using sbt build tool http://www.scala-sbt.org),

      sbt

Then use the following sbt commands


    update
    compile
    pack // creates binary runnable files that will be in target/pack/bin/benchmark-table

To use the tool do (we assume JAVA_HOME is correctly defined):

    target/pack/bin/benchmark-table label instance-path algo

where algo is CT/STR2/STR3/GAC4/GAC4R/MDD4R or AC5TCR

The output is line such as

    label	path	algo	timeinit(ms)	timesearch(ms)	overaltime(ms)	#bkts #nodes    #solutions

In case of troubles or questions please contact Pierre Schaus.

