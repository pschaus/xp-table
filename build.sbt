
enablePlugins(PackPlugin)


lazy val root = (project in file(".")).
  settings(
    name := "table-xp",
    scalaVersion := "2.12.4",
    javaOptions in run += "-Xmx4G",
    resolvers += "Oscar Snapshots" at "http://artifactory.info.ucl.ac.be/artifactory/libs-snapshot-local/",
    libraryDependencies += "oscar" %% "oscar-cp" % "4.1.0-SNAPSHOT" withSources(),
    libraryDependencies += "org.choco-solver" % "choco-solver" % "3.3.3-j7"
)